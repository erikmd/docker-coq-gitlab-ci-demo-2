# docker-coq-gitlab-ci-demo-2

[![pipeline status](https://gitlab.com/erikmd/docker-coq-gitlab-ci-demo-2/badges/master/pipeline.svg)](https://gitlab.com/erikmd/docker-coq-gitlab-ci-demo-2/commits/master)

Demo of [Docker-Coq](https://hub.docker.com/r/coqorg/coq/) + [GitLab CI](./.gitlab-ci.yml).
